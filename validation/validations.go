package validation

import (
	"errors"
	"strconv"

	s "github.com/emmanuel196/crud_gokit/services"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func IsEmpty(product s.Product) error {
	if product.Amount == "" {
		return errors.New("error, not valid amount")
	} else if product.Name == "" {
		return errors.New("error, not valid name")
	} else if product.Stok == "" {
		return errors.New("error, not valid stok")
	} else {
		return nil
	}
}

func IsNumber(product s.Product) error {
	_, errAmount := strconv.Atoi(product.Amount)
	_, errStok := strconv.Atoi(product.Stok)
	if errAmount != nil {
		return errors.New("error, amount is not a number")
	} else if errStok != nil {
		return errors.New("error, stok is not a number")
	} else {
		return nil
	}
}

func InvalidMongoId(id string) error {
	if !primitive.IsValidObjectID(id) {
		return errors.New("invalid id")
	}
	return nil
}

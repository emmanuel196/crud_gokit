package main

import (
	"context"
	"flag"
	"fmt"
	"os/signal"
	"syscall"

	"github.com/emmanuel196/crud_gokit/common"
	"github.com/emmanuel196/crud_gokit/database"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"net/http"
	"os"

	s "github.com/emmanuel196/crud_gokit/services"
	"go.mongodb.org/mongo-driver/mongo"
)

func main() {
	var httpAddr = flag.String("http", ":8000", "http listen address")
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger,
			"main", "caller",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	level.Info(logger).Log("message", "main started")
	defer level.Info(logger).Log("message", "main end")
	ctx := context.Background()

	var db *mongo.Database
	{
		var err error
		db, err = database.GetDB()
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
	}

	flag.Parse()

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	mux := http.NewServeMux()
	mux.Handle("/api/products/", s.NewHttpServer(ctx, s.Server(db, logger)))
	http.Handle("/", common.CommonMiddleware(mux))

	go func() {
		fmt.Println("listen on port", *httpAddr)
		errs <- http.ListenAndServe(*httpAddr, nil)
	}()
	level.Error(logger).Log("exit", <-errs)

}

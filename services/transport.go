package services

import (
	"context"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
)

func NewHttpServer(_ context.Context, endpoint Endpoint) http.Handler {
	r := mux.NewRouter()
	productRoute := r.PathPrefix("/api/products").Subrouter()
	productRoute.Methods("GET").Path("/products").Handler(httptransport.NewServer(
		endpoint.GetProducts,
		DecodeGetProducts,
		EncodeResponse,
	))

	productRoute.Methods("GET").Path("/product/{id}").Handler(httptransport.NewServer(
		endpoint.FindProduct,
		DecodeFindProduc,
		EncodeResponse,
	))

	productRoute.Methods("POST").Path("/add_product").Handler(httptransport.NewServer(
		endpoint.CreateProduct,
		DecodeCreateProduct,
		EncodeResponse,
	))

	productRoute.Methods("PUT").Path("/update_product/{id}").Handler(httptransport.NewServer(
		endpoint.UpdateProduct,
		DecodeUpdateProduct,
		EncodeResponse,
	))

	productRoute.Methods("DELETE").Path("/delete_product/{id}").Handler(httptransport.NewServer(
		endpoint.DeleteProduct,
		DecodeDeleteProduc,
		EncodeResponse,
	))

	return productRoute
}

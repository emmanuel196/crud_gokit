package services

import (
	"context"
	"errors"
	"time"

	"github.com/go-kit/kit/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type repository struct {
	db     *mongo.Collection
	logger log.Logger
}

func NewRepository(db *mongo.Collection, logger log.Logger) Repository {
	return &repository{
		db:     db,
		logger: log.With(logger, "repository", "sql product"),
	}
}

func (repository *repository) GetProducts(ctx context.Context) ([]Product, error) {
	var products []Product
	current, err := repository.db.Find(ctx, bson.M{})
	if err != nil {
		return nil, errors.New("unable to handle repo request")
	}
	for current.Next(ctx) {
		var product Product
		if err := current.Decode(&product); err != nil {
			return products, err
		}
		products = append(products, product)
	}
	if err := current.Err(); err != nil {
		return products, err
	}
	_ = current.Close(ctx)
	if len(products) == 0 {
		return products, mongo.ErrNoDocuments
	}
	return products, nil
}

func (repository *repository) FindProduct(ctx context.Context, id string) (Product, error) {

	product := Product{}
	ID, _ := primitive.ObjectIDFromHex(id)
	if err := repository.db.FindOne(ctx, bson.M{"_id": ID}).Decode(&product); err != nil {
		return product, err
	}
	return product, nil

}

func (repository *repository) CreateProduct(ctx context.Context, product Product) error {

	_, err := repository.db.InsertOne(ctx, product)
	if err != nil {
		return err
	}
	return nil
}

func (repository *repository) UpdateProduct(ctx context.Context, product Product, productId string) error {

	oid, _ := primitive.ObjectIDFromHex(productId)

	productAux:= Product{}
	if err := repository.db.FindOne(ctx, bson.M{"_id": oid}).Decode(&productAux); err != nil {
		return err
	}

	filter := bson.M{"_id": oid} //filtro para la busqueda
	update := bson.M{
		"$set": bson.M{
			"name":       product.Name,
			"amount":     product.Amount,
			"stok":       product.Stok,
			"updated_at": time.Now(),
		},
	}
	_, err := repository.db.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}
	return nil
}

func (repository *repository) DeleteProduct(ctx context.Context, productId string) error {

	ID, _ := primitive.ObjectIDFromHex(productId)

	productAux:= Product{}
	if err := repository.db.FindOne(ctx, bson.M{"_id": ID}).Decode(&productAux); err != nil {
		return err
	}

	res, err := repository.db.DeleteOne(ctx,
		bson.M{"_id": ID})

	if err != nil {
		return err
	}
	if res.DeletedCount == 0 {
		return errors.New("the product does not exist")
	}
	return nil
}
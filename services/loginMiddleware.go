package services

import (
	"context"
	"github.com/go-kit/kit/log"
	"time"
)

type loggingMiddleware struct {
	logger log.Logger
	next   Service
}

func (s loggingMiddleware) GetProducts(ctx context.Context) (p []Product, error error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetProducts",
			"input", ctx,
			"output", p,
			"err", error,
			"took", time.Since(begin),
		)
	}(time.Now())
	p, error = s.next.GetProducts(ctx)
	return
}

func (s loggingMiddleware) FindProduct(ctx context.Context, productId string) (Product, error) {
 return Product{}, nil
}

func (s loggingMiddleware) CreateProduct(ctx context.Context, name string, amount string, stok string) (string, error) {
	return "", nil
}

func (s loggingMiddleware) UpdateProduct(ctx context.Context, name string, amount string, stok string, productId string) (string, error) {
	return "", nil
}

func (s loggingMiddleware) DeleteProduct(ctx context.Context, productId string) (string, error) {
	return "", nil
}
package services

import "context"

type Service interface {
	GetProducts(ctx context.Context) ([]Product, error)
	FindProduct(ctx context.Context, productId string) (Product, error)
	CreateProduct(ctx context.Context, name string, amount string, stok string) (string, error)
	UpdateProduct(ctx context.Context, name string, amount string, stok string, productId string) (string, error)
	DeleteProduct(ctx context.Context, productId string) (string, error)
}

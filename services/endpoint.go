package services

import (
	"context"
	"github.com/go-kit/kit/endpoint"
)

type Endpoint struct {
	GetProducts   endpoint.Endpoint
	FindProduct   endpoint.Endpoint
	CreateProduct endpoint.Endpoint
	UpdateProduct endpoint.Endpoint
	DeleteProduct endpoint.Endpoint
}

func MakeEndpoints(s Service) Endpoint {
	return Endpoint{
		GetProducts:   makeGetProducts(s),
		FindProduct:   makeFindProduct(s),
		CreateProduct: makeCreateProduct(s),
		UpdateProduct: makeUpdateProduct(s),
		DeleteProduct: makeDeleteProduct(s),
	}
}

func makeGetProducts(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		products, err := s.GetProducts(ctx)
		var response GetProductsResponse
		if err != nil {
			response.Error = err.Error()
		} else {
			response.Error = ""
		}
		response.Products = products
		return response, nil
	}
}

func makeFindProduct(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(FindProductRequest)
		product, err := s.FindProduct(ctx, req.ProductId)
		var response FindProductResponse
		if err != nil {
			response.Error = err.Error()
		} else {
			response.Error = ""
		}
		response.Product = &product
		return response, nil
	}
}

func makeCreateProduct(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateProductRequest)
		ok, err := s.CreateProduct(ctx, req.Name, req.Amount, req.Stok)
		var response CreateProductResponse
		if err != nil {
			response.Error = err.Error()
		} else {
			response.Error = ""
		}
		response.Message = ok
		return response, nil
	}
}

func makeUpdateProduct(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateProductRequest)
		ok, err := s.UpdateProduct(ctx, req.Info.Name, req.Info.Amount, req.Info.Stok, req.ProductId)
		var response UpdateProductResponse
		if err != nil {
			response.Error = err.Error()
		} else {
			response.Error = ""
		}
		response.Message = ok
		return response, nil
	}
}

func makeDeleteProduct(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteProductRequest)
		ok, err := s.DeleteProduct(ctx, req.ProductId)
		var response DeleteProductResponse
		if err != nil {
			response.Error = err.Error()
		} else {
			response.Error = ""
		}
		response.Message = ok
		return response, nil
	}
}

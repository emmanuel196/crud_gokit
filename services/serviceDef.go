package services

import (
	"context"
	"errors"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type service struct {
	repository Repository
	logger     log.Logger
}

func NewService(repository Repository, logger log.Logger) Service {
	return &service{
		repository: repository,
		logger:     logger,
	}
}

func (s service) GetProducts(ctx context.Context) ([]Product, error) {
	logger := log.With(s.logger, "method", "GetProducts")
	products, err := s.repository.GetProducts(ctx)
	if err != nil {
		level.Error(logger).Log("err", err)
		return nil, errors.New("not products found")
	}
	logger.Log("GetProducts", "Success")
	return products, nil
}

func (s service) FindProduct(ctx context.Context, productId string) (Product, error) {
	logger := log.With(s.logger, "method", "FindProduct")
	product, err := s.repository.FindProduct(ctx, productId)
	if err != nil {
		level.Error(logger).Log("err", err)
		return product, err
	}
	logger.Log("FindProduct", "Success")
	return product, nil
}

func (s service) CreateProduct(ctx context.Context, name string, amount string, stok string) (string, error) {
	logger := log.With(s.logger, "method", "CreateProduct")
	product := Product{
		ID:     primitive.NewObjectID(),
		Name:   name,
		Amount: amount,
		Stok:   stok,
	}
	if err := s.repository.CreateProduct(ctx, product); err != nil {
		level.Error(logger).Log("err", err)
		return "", err
	}
	logger.Log("CreateProduct", "Success")
	return "product created successfully", nil
}

func (s service) UpdateProduct(ctx context.Context, name string, amount string, stok string, productId string) (string, error) {
	logger := log.With(s.logger, "method", "UpdateProduct")
	product := Product{
		ID:        primitive.NewObjectID(),
		Name:      name,
		Amount:    amount,
		Stok:      stok,
		UpdatedAt: time.Now(),
	}
	if err := s.repository.UpdateProduct(ctx, product, productId); err != nil {
		level.Error(logger).Log("err", err)
		return "", err
	}
	logger.Log("UpdateProduct", "Success")
	return "product updated successfully", nil
}

func (s service) DeleteProduct(ctx context.Context, productId string) (string, error) {
	logger := log.With(s.logger, "method", "DeleteProduct")
	if err := s.repository.DeleteProduct(ctx, productId); err != nil {
		level.Error(logger).Log("err", err)
		return "", err
	}
	logger.Log("DeleteProduct", "Success")
	return "product deleted successfully", nil
}

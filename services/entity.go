package services

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/mux"
)

type ProductAux struct {
	Name   string `json:"name"`
	Amount string `json:"amount"`
	Stok   string `json:"stok"`
}

type GetProductsResponse struct {
	Products []Product `json:"products,omitempty"`
	Error    string    `json:"error,omitempty"`
}

type FindProductRequest struct {
	ProductId string `json:"productId"`
}

type FindProductResponse struct {
	Product *Product `json:"product,omitempty"`
	Error   string   `json:"error,omitempty"`
}

type CreateProductRequest struct {
	Name   string `json:"name"`
	Amount string `json:"amount"`
	Stok   string `json:"stok"`
}

type CreateProductResponse struct {
	Message string `json:"message,omitempty"`
	Error   string `json:"error,omitempty"`
}

type UpdateProductRequest struct {
	Info      ProductAux `json:"info"`
	ProductId string     `json:"productId"`
}

type UpdateProductResponse struct {
	Message string `json:"message,omitempty"`
	Error   string `json:"error,omitempty"`
}

type DeleteProductRequest struct {
	ProductId string `json:"productId"`
}

type DeleteProductResponse struct {
	Message string `json:"message,omitempty"`
	Error   string `json:"error,omitempty"`
}

func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func DecodeGetProducts(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func DecodeFindProduc(_ context.Context, r *http.Request) (interface{}, error) {
	var req FindProductRequest
	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		return nil, errors.New("bad request id is required")
	}
	req.ProductId = id
	return req, nil
}

func DecodeCreateProduct(_ context.Context, r *http.Request) (interface{}, error) {
	var req CreateProductRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	return req, nil
}

func DecodeUpdateProduct(_ context.Context, r *http.Request) (interface{}, error) {
	var req UpdateProductRequest
	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		return nil, errors.New("bad request id is required")
	}
	req.ProductId = id
	if err := json.NewDecoder(r.Body).Decode(&req.Info); err != nil {
		return nil, err
	}
	return req, nil
}

func DecodeDeleteProduc(_ context.Context, r *http.Request) (interface{}, error) {
	var req DeleteProductRequest
	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		return nil, errors.New("bad request id is required")
	}
	req.ProductId = id
	return req, nil
}

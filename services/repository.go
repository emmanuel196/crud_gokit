package services

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Product struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Name      string             `json:"name"`
	Amount    string             `json:"amount"`
	Stok      string             `json:"stok"`
	CreatedAt time.Time          `bson:"created_At" json:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at" json:"updated_at,omitempty"`
}

type Repository interface {
	GetProducts(ctx context.Context) ([]Product, error)
	FindProduct(ctx context.Context, productId string) (Product, error)
	CreateProduct(ctx context.Context, product Product) error
	UpdateProduct(ctx context.Context, product Product, productId string) error
	DeleteProduct(ctx context.Context, productId string) error
}

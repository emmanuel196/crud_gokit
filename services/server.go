package services

import (
	"github.com/emmanuel196/crud_gokit/common"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"go.mongodb.org/mongo-driver/mongo"
)

func Server(db *mongo.Database, logger log.Logger) Endpoint {
	{
		logger = log.With(logger,
			"service", "product",
			"time", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}



	level.Info(logger).Log("message", "service started")
	var srv Service

	{

		productCollection := db.Collection(common.GetConf("COLLECTION"))
		repository := NewRepository(productCollection, logger)
		srv = NewService(repository, logger)
		srv = loggingMiddleware{logger, srv}
	}
	return MakeEndpoints(srv)
}

module github.com/emmanuel196/crud_gokit

go 1.16

require (
	github.com/go-kit/kit v0.11.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/spf13/viper v1.8.1
	go.mongodb.org/mongo-driver v1.6.0 // indirect
)
